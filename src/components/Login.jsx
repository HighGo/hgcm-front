import React from "react"
import '../css/login.css';
import reqwest from 'reqwest';
import logologin from "../img/logo-hgcloud.png"


export default class HLogin extends React.Component {
    constructor() {
        super();
        this.state = {
            userName: '',
            password: '',
            errmessage: ''
        }
    }

    settingState(obj) {
        this.setState(obj);
    }

	
	
	login(event){
		event.preventDefault();
		
        if (this.state.userName === null || this.state.userName === undefined || this.state.userName.trim() === '') {
            this.settingState({errmessage: '用户名不能为空。'})
            return;
        }
		
		if (this.state.password === null || this.state.password === undefined || this.state.password.trim() === '') {
            this.settingState({errmessage: '密码不能为空。'})
            return;
        }
		
		const that = this;
		var mainIp = document.location.hostname;
		var port = document.location.port;
		
		if(port === ''){
			port = 80;
		}
		var url ='http://' + mainIp + ':'+port+'/hgmonitor/userlogin';
		
		reqwest({
                    url: url
                    , method: 'post'
                    , type: 'json'
                    , data: {username:that.state.userName,password:that.state.password}
                    , contentType: 'application/x-www-form-urlencoded'
                    , success: function (resp) {
						
						if(resp.success=="false"){
							that.settingState({errmessage:resp.errMessage})
						}else{
							localStorage.setItem("HData_UserName", that.state.userName)
							that.props.history.push("/hgmonitor");
						}
                    }, error: function (err) {
                        that.settingState({errmessage:'连接服务器失败'})
                    }
                }
            );
		
        //加密
        //console.log(this.state.password + ":" + SHA256(this.state.password));

        that.refs.userName.focus();
	}

    render() {
        return (
            <div className="login-bg">
                <div className="login-tbl">
                    <form onSubmit={this.login.bind(this)} method="post">
                        <img src={logologin}/>

                        <input name="name" placeholder="用户名" value={this.state.userName}
                               onChange={e => this.settingState({userName: e.target.value})} ref="userName"/>

                        <input type="password" name="password" placeholder="密码" value={this.state.password}
                               onChange={e => this.settingState({password: e.target.value})}/>
                        <button className="login-btn" type="submit">登录</button>
                    </form>

                    <span className="login-err"><strong>{this.state.errmessage}</strong></span>
                </div>
            </div>
        )
    }
}
