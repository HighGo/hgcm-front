import React from 'react';

import LogTop from '../../img/logo-top.png';

import EditableTable from './EditableTable';
import {Select} from 'antd';
import '../../css/cloud.css'

import Echarts from './Echarts';

import {Route,Link} from 'react-router-dom';

export default class CloudMain extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            onLine:0,
            offLine:0,
            refreshTime: 'one',
        };
        console.log('cloudMain......')
        console.log(this.props.history)
    };
    handleChange = (e) =>{
        console.log('CloudMain');
        this.refs.table.handleChange(e);
    }
    handChangeOnLine =(a,b) =>{
        this.setState({
            onLine:a,
            offLine:b
        })
    }
	onExit(){
		localStorage.removeItem('HData_UserName');
		console.log(this);
		window.location.hash='/';
		//this.history.go("/")
	}
    render(){
        return(
            <div className="container">
                <div className="head ant-layout-header" style={{padding: '0px 5px'}}>
                    <div className="logo">
                        <img src={LogTop} alt=""/>
                    </div>
                    <div className="exit" >{localStorage.getItem("HData_UserName")}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <span onClick={this.onExit}>退出</span>
                    </div>
                </div>
                <div className="refresh-time">
                        在线：{this.state.onLine}&nbsp;&nbsp;

                        离线：{this.state.offLine}&nbsp;&nbsp;
                        &nbsp;&nbsp;刷新频率：
                        <Select defaultValue={this.state.refreshTime} style={{ width: 120 }} onChange={this.handleChange}>

                                <Option value="one">一分钟</Option>
                                <Option value="two">五分钟</Option>
                                <Option value="three">十分钟</Option>

                        </Select>
                </div>
                <ul>
                    <li><Link to="/cloud">Echarts</Link></li>
                    <li><Link to="/cloud/calc/1">EditableTable</Link></li>
                </ul>
                <hr/>
                {/*<Echarts history={this.props.history}/>*/}


                <Route exact path="/cloud" component={Echarts}/>
                <Route path="/cloud/calc/:id" component={EditableTable}/>
            </div>
        )
    }
}