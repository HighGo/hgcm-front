import React from 'react';
import {Table, Popconfirm,Icon} from 'antd';
import EditableCell from './EditableCell';
import reqwest from 'reqwest';


import Add from '../../img/icon-add.png'
import Delete from '../../img/icon-delete.png'
import MainErrorIcon from '../../img/icon-status1.png'
import NormalIcon from '../../img/icon-status2.png'
import DbErrorIcon from '../../img/icon-status3.png'

var mainIp = document.location.hostname;
var port = document.location.port;

if(port === ''){
    port = 80;
}
var url ='http://' + mainIp + ':'+port+'/hgmonitor/hostaddress';

export default class EditableTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            count: 0,
            addButtonShow: true,
            loading: false,
            lock:false
        };
        // console.log(url)
        console.log('EditableTable.....')
        console.log(this.props)
    };
    _handOnClick = () => {
        console.log(this.props);
        const history = this.props.history;
        history.push('/');
    };
    componentDidMount = () => {//DOM被渲染出来后调用,用于赋值
        this.getAllDate();

        // 定时器，动态加载
        // this.timerID = setInterval(
        //     () => this.setDataSource(),
        //     15000
        // );
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    // 修改定时器时间
    handleChange = (time) => {
        // points="0,20 10,30 20,25 30,10 40,20 50,30 60,25 70,10"/>
        console.log('table');

        // var polyline = document.getElementById('polygon');
        //
        // polyline.points.removeItem(0);
        // for (var i = 0; i < polyline.points.length; i++) {
        //     polyline.points[i].x = polyline.points[i].x - 10;
        //
        // }
        //
        // const random = Math.random()*15;
        // console.log(random)
        //
        // var points = polyline.getAttribute("points");
        // points += " 70, "+random;
        // console.log(points)
        // polyline.setAttribute("points", points);
        // console.log(polyline.getAttribute("points"))

        // t.setAttribute("points", "0,30 10,10 20,25 30,10 40,20 50,30 60,35 70,30");
        // const list = t.points;
        // console.log(list)
        // list.forEach(function (item,index) {
        //     console.log(item.x)
        //     // list.push("");
        // })

        // aa.getBBox().height = 30
        // aa.setAttribute('points', '0,20 10,30 20,25 30,10 40,20 50,30 60,25 70,30')

        // if (time === 'one') {
        //     clearInterval(this.timerID);
        //     this.timerID = setInterval(
        //         () => this.setDataSource(),
        //         1000
        //     );
        // } else if (time === 'two') {
        //     clearInterval(this.timerID);
        //     this.timerID = setInterval(
        //         () => this.setDataSource(),
        //         10000
        //     );
        // } else {
        //     clearInterval(this.timerID);
        //     this.timerID = setInterval(
        //         () => this.setDataSource(),
        //         500000
        //     );
        // }
    }
    ;
    onCellChange = (index, record, key) => {
        const that = this;
        const {dataSource} = this.state;
        return (value) => {
            reqwest({
                    url: url
                    , method: 'post'
                    , type: 'json'
                    , data: {hostaddress: value, key: record.key}
                    , contentType: 'application/x-www-form-urlencoded'
                    , success: function (resp) {
                        dataSource[index].hostaddress = value;
                        that.setState({
                            dataSource: [...dataSource],
                        });
                        // that.setDataSource();//获取表格数据
                    }, error: function (err) {
                        console.log('onCellChange,,error')
                        console.log(err)
                    }
                }
            );
        };
    }
    onDelete = (index, record) => {
        const that = this;
        const {dataSource} = this.state;

        //删除key
        reqwest({
                url: url
                , method: 'post'
                , type: 'json'
                , data: {key: record.key}
                , contentType: 'application/x-www-form-urlencoded'
                , success: function (resp) {

                    if(that.state.lock){//lock==true，说明正在循环
                        dataSource[index].lock = true;
                        that.setState({//循环时删除，要把删除图标改为等待图标
                            dataSource: dataSource
                        });
                        const time = setInterval(function () {//每秒监听是否循环完毕，循环完毕执行删除操作
                            console.log('ok-----------------'+that.state.lock);
                            if(that.state.lock === false){
                                dataSource.splice(index, 1);//splice方法参数(移除的下标，移除几列)
                                that.setState({
                                    dataSource: [...dataSource],
                                    loadingShow: false
                                });
                                clearInterval(time);
                            }
                        },1000);
                    }else{
                        dataSource.splice(index, 1);//splice方法参数(移除的下标，移除几列)
                        that.setState({
                            dataSource: [...dataSource],
                        });
                    }
                }, error: function (err) {
                    console.log('error')
                    console.log(err)
                }
            }
        );
    };
    handleAdd = () => {
        this.setState({
            addButtonShow: false
        });

        const that = this;
        const {count, dataSource} = this.state;
        const newData = {
            // key:count,
            index: count,
            status: 2,
            hostaddress: `192.168.100.100`,
            lock:false
            // dataSize: 20.15
        };

        // console.log(newData)

        reqwest({
                url: url
                , method: 'post'
                , type: 'json'
                , data: {hostaddress: newData.hostaddress}
                , contentType: 'application/x-www-form-urlencoded'
                , success: function (resp) {
                    console.log(resp)
                    // that.getAllDate();//刷新列表
                    if (resp.success) {

                        if(that.state.lock){//lock==true，说明正在循环
                            const time = setInterval(function () {//每秒监听是否循环完毕，循环完毕执行删除操作
                                if(that.state.lock === false){
                                    newData.key = resp.key
                                    that.setState({
                                        dataSource: [...dataSource, newData],
                                        count: count + 1,
                                        addButtonShow: true
                                    });
                                    clearInterval(time);
                                }
                            },1000);
                        }else{
                            newData.key = resp.key
                            that.setState({
                                dataSource: [...dataSource, newData],
                                count: count + 1,
                                addButtonShow: true
                            });
                        }
                        // that.setDataSource();//获取表格数据
                    }
                }, error: function (err) {
                    console.log('error')
                    console.log(err)
                }
            }
        );

        // this.setState({
        //     dataSource: [...dataSource, newData],
        //     count: count + 1,
        //     addButtonShow: true
        // });

    };

    getAllDate() {//获取全部数据
        const that = this;
        reqwest({
                url: url
                , method: 'get'
                , type: 'json'
                , contentType: 'application/x-www-form-urlencoded'
                , success: function (resp) {
                    console.log(resp)
                    that.setState({
                        dataSource: resp
                    });
                    // that.setDataSource();//获取表格数据

                }, error: function (err) {
                    console.log('getAllDate,,error')
                    console.log(err)
                }
            }
        );
    }

    setDataSource() {
        console.log('setDataSource')

        this.setState({//循环开始加锁，循环后解锁
            lock: true
        })
        let that = this;
        const dataSource = [...this.state.dataSource];

        console.log(dataSource)
        var onNumber = 0;
        var offNumber = 0;

        dataSource.forEach(function (item, index) {
            const requestUrl = 'http://' + item.hostaddress + ':'+port+'/hgcloud/api/getInstanceInfo';
            //console.log(requestUrl)
            reqwest({
                    url: requestUrl
                    , method: 'get'
                    , type: 'json'
                    , contentType: 'application/json'
                    , timeout: '1000'
                    , success: function (resp) {
                        if (resp.success) {
                            //正常返回true
                            onNumber = onNumber + 1;//正常统计+1
                            that.props.handChangeOnLine(onNumber, offNumber);//更新统计

                            item.status = 0;
                            item.connSize = resp.data.connSize;
                            item.dataSize = resp.data.dataSize;
                            item.xlogSize = resp.data.xlogSize;
                            that.setState({dataSource});//更新表格数据

                        } else {
                            //api返回false,连接数 和 datasize是小于0,服务器断开
                            offNumber += 1;//异常统计+1
                            that.props.handChangeOnLine(onNumber, offNumber);//更新统计

                            item.status = 1;
                            that.setState({dataSource});//更新表格数据
                        }
                        if (index === dataSource.length - 1) {//解锁lock
                            that.setState({
                                lock:false
                            })
                        }
                    }, error: function (err) {
                        //请求失败，请求超时
                        console.log('setDataSource ,, err')
                        console.log(err)

                        offNumber = offNumber + 1;//异常统计+1
                        that.props.handChangeOnLine(onNumber, offNumber);//更新统计

                        //因为数据库断开优先级较高，可以先设置为数据库断开，然后判断tomcat是否断开
                        //查看是否tomcat服务正常，意义在于断开情况
                        const tomcatURL = 'http://' + item.hostaddress + ':'+port;
                        reqwest({
                                url: tomcatURL
                                , method: 'get'
                                , contentType: 'application/json'
                                , timeout: '1000'
                                , success: function (resp) {
                                    console.log('getLocalhostStatus,,success');//tomcat请求正常
                                    item.status = 2;//tomcat正常，数据库不正常
                                    that.setState({dataSource})//更新表格数据

                                    if (index === dataSource.length - 1) {//解锁lock
                                        that.setState({
                                            lock:false
                                        })
                                    }
                                }, error: function (err) {
                                    // console.log('getLocalhostStatus,,err');//tomcat请求失败
                                    console.log(err)

                                    item.status = 1;//tomcat不正常
                                    that.setState({dataSource})//更新表格数据

                                    if (index === dataSource.length - 1) {//解锁lock
                                        that.setState({
                                            lock:false
                                        })
                                    }
                                }
                            }
                        );
                    }
                }
            );
        });
    };

    render() {
        const columns = [
            {
                title: '编号',
                dataIndex: 'index',
                width: '10%',
                render: (text, record, index) => {
                    return index + 1;
                },
            }, {
                title: '状态',
                dataIndex: 'status',
                width: '10%',
                render: (text, record, index) => {
                    if (text === 0) {
                        return <img alt="" src={NormalIcon}/>;
                    } else if (text === 1) {
                        return <img alt="" src={MainErrorIcon}/>;
                    } else {
                        return <img alt="" src={DbErrorIcon}/>;
                    }
                },
            },
            {
                title: 'IP地址',
                dataIndex: 'hostaddress',
                width: '20%',
                render: (text, record, index) => (
                    <EditableCell value={text} onChange={this.onCellChange(index, record, 'name')}/>
                ),

            }, {
                title: '链接数',
                dataIndex: 'connSize',
                width: '15%',
            }, {
                title: '数据库大小(MB)',
                dataIndex: 'dataSize',
                width: '15%',
                render: (text, record, index) => {
                    // const id = "polygon"+{record.key};
                    return <div>
                        {text}
                        {/*<svg width="70%" height="30px" style={{marginLeft: '5px'}}*/}
                        {/*xmlns="http://www.w3.org/2000/svg">*/}
                        {/*<polyline fill="none" stroke="black" id='polygon'*/}
                        {/*points="0,20 10,30 20,25 30,10 40,20 50,30 60,25 70,10"/>*/}
                        {/*</svg>*/}
                    </div>
                }
            }, {
                title: 'xLog大小(MB)',
                dataIndex: 'xlogSize',
                width: '15%',
            }, {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record, index) => {
                    if (record.lock) {
                        return <Icon type="loading" />;
                    } else {
                        return <Popconfirm title="确定要删除吗?" onConfirm={() => this.onDelete(index, record)}>
                            <img alt="" src={Delete}/>
                        </Popconfirm>
                    }
                },
            }];

        //网页可见区域高
        let height = document.body.clientHeight;
        let minHeight = height - 210;

        return (
            <div className="table">
                <button onClick={this._handOnClick}>测试按钮</button>
                <Table
                    bordered
                    dataSource={this.state.dataSource}
                    columns={columns}
                    showHeader={true}
                    pagination={false}
                    size="small"
                    rowKey="key"
                    scroll={{y: minHeight - 30}}
                    style={{minHeight: minHeight}}

                />
                <div className="addLine">
                    <div className="item1">
                        {this.state.addButtonShow?
                            <img alt="" src={Add} onClick={this.handleAdd}/>:
                            <Icon type="loading" />}
                    </div>
                    <img alt="" src={NormalIcon}/>
                    <div className="item">连接正常</div>
                    <img alt="" src={MainErrorIcon}/>
                    <div className="item">主机断开</div>
                    <img alt="" src={DbErrorIcon}/>
                    <div className="item">服务器断开</div>
                </div>
            </div>
        );
    }
}
