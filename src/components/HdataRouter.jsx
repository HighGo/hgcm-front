import React from "react";
import {HashRouter, Route,BrowserRouter} from "react-router-dom";
import {createHashHistory} from "history";
import Login from "./Login";
import CloudMain from "./cloud/CloudMain";

// const history = createHashHistory();
//
// history.listen(location => {
//     if (location.pathname !== "/") {
//         if (localStorage.getItem("HData_UserName") === null || localStorage.getItem("HData_UserName") === undefined) {
//             history.push("/");
//         }
//     }
// });

export default class HdataRouter extends React.Component {
    constructor() {
        super();
		
        // const url = history.location.pathname;
        // console.log(url)
        // if (localStorage.getItem("HData_UserName") === null || localStorage.getItem("HData_UserName") === undefined) {
        //     if (url !== '/') {
        //         history.push("/")
        //     }
        // } else {
        //     if (url !== '/hgmonitor') {
        //         history.push("/hgmonitor");
        //     }
        // }
        console.log('HdataRouter...........');
    }

    render() {
        return (
            <BrowserRouter >
                <div>
                    <Route exact path="/" component={Login}/>
                    <Route path="/cloud" component={CloudMain}/>
                </div>
            </BrowserRouter>
        )
    }
}
