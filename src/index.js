import React from 'react';
import ReactDOM from 'react-dom';
import HdataRouter from './components/HdataRouter';
import CloudMain from './components/cloud/CloudMain';

require('es6-promise').polyfill();
document.title = 'HG Cloud 云监控系统';

ReactDOM.render(
    <HdataRouter/>
    , document.getElementById('root')
);

